FROM registry.gitlab.com/will-capstone/greenhouse-direct/ubuntu-bionic:latest

RUN /bin/bash -c 'apt-get update;\
apt-get -y install curl wget maven zip unzip apt-transport-https ca-certificates gnupg lsb-release;\
mkdir new_tmp;\
cd new_tmp;\
curl -O https://download.java.net/java/GA/jdk15.0.2/0d1cfde4252546c6931946de8db48ee2/7/GPL/openjdk-15.0.2_linux-x64_bin.tar.gz;\
mkdir /usr/lib/java;\
tar -xvf openjdk-15.0.2_linux-x64_bin.tar.gz -C /usr/lib/java;\
sed -i "s/bin\"/bin:\/usr\/lib\/java\/jdk-15.0.2\/bin\"\nJAVA_HOME=\"\/usr\/lib\/java\/jdk-15.0.2/g" /etc/environment;\
update-alternatives --install "/usr/bin/java" "java" "/usr/lib/java/jdk-15.0.2/bin/java" 0;\
update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/java/jdk-15.0.2/bin/javac" 0;\
update-alternatives --install "/usr/bin/javap" "javap" "/usr/lib/java/jdk-15.0.2/bin/javap" 0;\
update-alternatives --set java /usr/lib/java/jdk-15.0.2/bin/java;\
update-alternatives --set javac /usr/lib/java/jdk-15.0.2/bin/javac;\
update-alternatives --set javap /usr/lib/java/jdk-15.0.2/bin/javap;\
java --version;\
wget https://github.com/pmd/pmd/releases/download/pmd_releases%2F6.34.0/pmd-bin-6.34.0.zip;\
unzip pmd-bin-6.34.0.zip;\
cp -r pmd-bin-6.34.0 /usr/local/share/;\
echo "alias pmd=\"/usr/local/share/pmd-bin-6.34.0/bin/run.sh pmd\"" >> ~/.bashrc;\
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg;\
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null;\
apt-get update;\
apt-get -y install docker-ce docker-ce-cli containerd.io'

CMD [ "bash" ]
